import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  server: {
    host: '127.0.0.1',
    proxy: {
     '/api': {
       target: 'http://localhost:8080', // 你的后端服务地址
       changeOrigin: true,
       rewrite: (path) => path.replace(/^\/api/, '') // 可选，重写路径
     }
    }
  },
  build: {
    rollupOptions: {
        input: {
            index: path.resolve(__dirname, 'index.html'),
            about: path.resolve(__dirname, 'admin.html'),
        }, output: {
            chunkFileNames: 'static/js/[name]-[hash].js',
            entryFileNames: "static/js/[name]-[hash].js",
            assetFileNames: "static/[ext]/name-[hash].[ext]"
        }
    }
  }
})
