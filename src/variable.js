export const options = {
                subject: [
                    { label: "数学", value: "0" },
                    { label: "语文", value: "1" },
                    { label: "英语", value: "2" },
                    { label: "物理", value: "3" },
                    { label: "化学", value: "4" },
                    { label: "生物", value: "5" },
                    { label: "政治", value: "6" },
                    { label: "历史", value: "7" },
                    { label: "地理", value: "8" },
                    { label: "音乐", value: "9" },
                    { label: "美术", value: "10" },
                    { label: "体育", value: "11" },
                    { label: "信息技术", value: "12" }
                ],
                sex: [
                    { label: "男", value: "0" },
                    { label: "女", value: "1" },
                ],
                region: [
                    {
                        label: "北京", value: "Beijing",
                        children:
                        [
                            { label: "北京", value: "Beijing" }
                        ]
                    },
                    {
                        label: "天津", value: "Tianjin",
                        children:
                            [
                                { label: "天津", value: "Tianjin" }
                            ]
                    },
                    {
                        label: "河北", value: "Hebei",
                        children:
                            [
                                { label: "石家庄", value: "Shijiazhuang" },
                                { label: "唐山", value: "Tangshan" },
                                { label: "秦皇岛", value: "Qinhuangdao" },
                                { label: "邯郸", value: "Handan" },
                                { label: "邢台", value: "Xingtai" },
                                { label: "保定", value: "Baoding" },
                                { label: "张家口", value: "Zhangjiakou" },
                                { label: "承德", value: "Chengde" },
                                { label: "沧州", value: "Cangzhou" },
                                { label: "廊坊", value: "Langfang" },
                                { label: "衡水", value: "Hengshui" }
                            ]
                    },
                    {
                        label: "山西", value: "Shanxi",
                        children:
                            [
                                { label: "太原", value: "Taiyuan" },
                                { label: "大同", value: "Datong" },
                                { label: "阳泉", value: "Yangquan" },
                                { label: "长治", value: "Changzhi" },
                                { label: "晋城", value: "Jincheng" },
                                { label: "朔州", value: "Shuozhou" },
                                { label: "晋中", value: "Jinzhong" },
                                { label: "运城", value: "Yuncheng" },
                                { label: "忻州", value: "Xinzhou" },
                                { label: "临汾", value: "Linfen" },
                                { label: "吕梁", value: "Lvliang" }
                            ]
                    },
                    {
                        label: "内蒙古", value: "Inner Mongolia",
                        children:
                            [
                                { label: "呼和浩特", value: "Hohhot" },
                                { label: "包头", value: "Baotou" },
                                { label: "乌海", value: "Wuhai" },
                                { label: "赤峰", value: "Chifeng" },
                                { label: "通辽", value: "Tongliao" },
                                { label: "鄂尔多斯", value: "Ordos" },
                                { label: "呼伦贝尔", value: "Hulun Buir" },
                                { label: "巴彦淖尔", value: "Bayannur" },
                                { label: "乌兰察布", value: "Ulanqab" },
                                { label: "兴安盟", value: "Hinggan" },
                                { label: "锡林郭勒盟", value: "Xilin Gol" },
                                { label: "阿拉善盟", value: "Alxa" }
                            ]
                    },
                    {
                        label: "辽宁", value: "Liaoning",
                        children:
                            [
                                { label: "沈阳", value: "Shenyang" },
                                { label: "大连", value: "Dalian" },
                                { label: "鞍山", value: "Anshan" },
                                { label: "抚顺", value: "Fushun" },
                                { label: "本溪", value: "Benxi" },
                                { label: "丹东", value: "Dandong" },
                                { label: "锦州", value: "Jinzhou" },
                                { label: "营口", value: "Yingkou" },
                                { label: "阜新", value: "Fuxin" },
                                { label: "辽阳", value: "Liaoyang" },
                                { label: "盘锦", value: "Panjin" },
                                { label: "铁岭", value: "Tieling" },
                                { label: "朝阳", value: "Chaoyang" },
                                { label: "葫芦岛", value: "Huludao" }
                            ]
                    },
                    {
                        label: "吉林", value: "Jilin",
                        children:
                            [
                                { label: "长春", value: "Changchun" },
                                { label: "吉林", value: "Jilin" },
                                { label: "四平", value: "Siping" },
                                { label: "辽源", value: "Liaoyuan" },
                                { label: "通化", value: "Tonghua" },
                                { label: "白山", value: "Baishan" },
                                { label: "松原", value: "Songyuan" },
                                { label: "白城", value: "Baicheng" },
                                { label: "延边", value: "Yanbian" }
                            ]
                    },
                    {
                        label: "黑龙江", value: "Heilongjiang",
                        children:
                            [
                                { label: "哈尔滨", value: "Harbin" },
                                { label: "齐齐哈尔", value: "Qiqihar" },
                                { label: "鸡西", value: "Jixi" },
                                { label: "鹤岗", value: "Hegang" },
                                { label: "双鸭山", value: "Shuangyashan" },
                                { label: "大庆", value: "Daqing" },
                                { label: "伊春", value: "Yichun" },
                                { label: "佳木斯", value: "Jiamusi" },
                                { label: "七台河", value: "Qitaihe" },
                                { label: "牡丹江", value: "Mudanjiang" },
                                { label: "黑河", value: "Heihe" },
                                { label: "绥化", value: "Suihua" },
                                { label: "大兴安岭", value: "Da Hinggan Ling" }
                            ]
                    },
                    {
                        label: "上海", value: "Shanghai",
                        children:
                            [
                                { label: "上海", value: "Shanghai" }
                            ]
                    },
                    {
                        label: "江苏", value: "Jiangsu",
                        children:
                            [
                                { label: "南京", value: "Nanjing" },
                                { label: "无锡", value: "Wuxi" },
                                { label: "徐州", value: "Xuzhou" },
                                { label: "常州", value: "Changzhou" },
                                { label: "苏州", value: "Suzhou" },
                                { label: "南通", value: "Nantong" },
                                { label: "连云港", value: "Lianyungang" },
                                { label: "淮安", value: "Huaian" },
                                { label: "盐城", value: "Yancheng" },
                                { label: "扬州", value: "Yangzhou" },
                                { label: "镇江", value: "Zhenjiang" },
                                { label: "泰州", value: "Taizhou" },
                                { label: "宿迁", value: "Suqian" }
                            ]
                    },
                    {
                        label: "浙江", value: "Zhejiang",
                        children:
                            [
                                { label: "杭州", value: "Hangzhou" },
                                { label: "宁波", value: "Ningbo" },
                                { label: "温州", value: "Wenzhou" },
                                { label: "嘉兴", value: "Jiaxing" },
                                { label: "湖州", value: "Huzhou" },
                                { label: "绍兴", value: "Shaoxing" },
                                { label: "金华", value: "Jinhua" },
                                { label: "衢州", value: "Quzhou" },
                                { label: "舟山", value: "Zhoushan" },
                                { label: "台州", value: "Taizhou" },
                                { label: "丽水", value: "Lishui" }
                            ]
                    },
                    {
                        label: "安徽", value: "Anhui",
                        children:
                            [
                                { label: "合肥", value: "Hefei" },
                                { label: "芜湖", value: "Wuhu" },
                                { label: "蚌埠", value: "Bengbu" },
                                { label: "淮南", value: "Huainan" },
                                { label: "马鞍山", value: "Ma'anshan" },
                                { label: "淮北", value: "Huaibei" },
                                { label: "铜陵", value: "Tongling" },
                                { label: "安庆", value: "Anqing" },
                                { label: "黄山", value: "Huangshan" },
                                { label: "滁州", value: "Chuzhou" },
                                { label: "阜阳", value: "Fuyang" },
                                { label: "宿州", value: "Suzhou" },
                                { label: "六安", value: "Lu'an" },
                                { label: "亳州", value: "Bozhou" },
                                { label: "池州", value: "Chizhou" },
                                { label: "宣城", value: "Xuancheng" }
                            ]
                    },

                    {
                        label: "福建", value: "Fujian",
                        children:
                            [
                                { label: "福州", value: "Fuzhou" },
                                { label: "厦门", value: "Xiamen" },
                                { label: "莆田", value: "Putian" },
                                { label: "三明", value: "Sanming" },
                                { label: "泉州", value: "Quanzhou" },
                                { label: "漳州", value: "Zhangzhou" },
                                { label: "南平", value: "Nanping" },
                                { label: "龙岩", value: "Longyan" },
                                { label: "宁德", value: "Ningde" },
                                { label: "平潭", value: "Pingtan" },
                                { label: "福清", value: "Fuqing" },
                                { label: "长乐", value: "Changle" },
                                { label: "连江", value: "Lienchiang"},
                                { label: "金门", value: "Kinmen"},
                                { label: "晋江", value: "Jinjiang"},
                                { label: "南安", value: "Nan'an"},
                                { label: "龙海", value: "Longhai"},
                                { label: "漳浦", value: "Zhangpu"},
                                { label: "诏安", value: "Zhao'an"},
                                { label: "东山", value: "Dongshan"},
                                { label: "云霄", value: "Yunxiao"},
                                { label: "华安", value: "Hua'an"},
                                { label: "长汀", value: "Changting"},
                                { label: "永定", value: "Yongding"},
                                { label: "上杭", value: "Shanghang"},
                                { label: "武平", value: "Wuping"},
                                { label: "连城", value: "Liancheng"},
                                { label: "福安", value: "Fu'an"},
                                { label: "福鼎", value: "Fuding"}

                            ]
                    },
                    {
                        label: "江西", value: "Jiangxi",
                        children:
                            [
                                { label: "南昌", value: "Nanchang" },
                                { label: "景德镇", value: "Jingdezhen" },
                                { label: "萍乡", value: "Pingxiang" },
                                { label: "九江", value: "Jiujiang" },
                                { label: "新余", value: "Xinyu" },
                                { label: "鹰潭", value: "Yingtan" },
                                { label: "赣州", value: "Ganzhou" },
                                { label: "吉安", value: "Ji'an" },
                                { label: "宜春", value: "Yichun" },
                                { label: "抚州", value: "Fuzhou" },
                                { label: "上饶", value: "Shangrao" }
                            ]
                    },
                    {
                        label: "山东", value: "Shandong",
                        children:
                            [
                                { label: "济南", value: "Jinan" },
                                { label: "青岛", value: "Qingdao" },
                                { label: "淄博", value: "Zibo" },
                                { label: "枣庄", value: "Zaozhuang" },
                                { label: "东营", value: "Dongying" },
                                { label: "烟台", value: "Yantai" },
                                { label: "潍坊", value: "Weifang" },
                                { label: "济宁", value: "Jining" },
                                { label: "泰安", value: "Taian" },
                                { label: "威海", value: "Weihai" },
                                { label: "日照", value: "Rizhao" },
                                { label: "莱芜", value: "Laiwu" },
                                { label: "临沂", value: "Linyi" },
                                { label: "德州", value: "Dezhou" },
                                { label: "聊城", value: "Liaocheng" },
                                { label: "滨州", value: "Binzhou" },
                                { label: "菏泽", value: "Heze" }
                            ]
                    },
                    {
                        label: "河南", value: "Henan",
                        children:
                            [
                                { label: "郑州", value: "Zhengzhou" },
                                { label: "开封", value: "Kaifeng" },
                                { label: "洛阳", value: "Luoyang" },
                                { label: "平顶山", value: "Pingdingshan" },
                                { label: "安阳", value: "Anyang" },
                                { label: "鹤壁", value: "Hebi" },
                                { label: "新乡", value: "Xinxiang" },
                                { label: "焦作", value: "Jiaozuo" },
                                { label: "濮阳", value: "Puyang" },
                                { label: "许昌", value: "Xuchang" },
                                { label: "漯河", value: "Luohe" },
                                { label: "三门峡", value: "Sanmenxia" },
                                { label: "南阳", value: "Nanyang" },
                                { label: "商丘", value: "Shangqiu" },
                                { label: "信阳", value: "Xinyang" },
                                { label: "周口", value: "Zhoukou" },
                                { label: "驻马店", value: "Zhumadian" },
                                { label: "济源", value: "Jiyuan" }
                            ]
                    },
                    {
                        label: "湖北", value: "Hubei",
                        children:
                            [
                                { label: "武汉", value: "Wuhan" },
                                { label: "黄石", value: "Huangshi" },
                                { label: "十堰", value: "Shiyan" },
                                { label: "宜昌", value: "Yichang" },
                                { label: "襄阳", value: "Xiangyang" },
                                { label: "鄂州", value: "Ezhou" },
                                { label: "荆门", value: "Jingmen" },
                                { label: "孝感", value: "Xiaogan" },
                                { label: "荆州", value: "Jingzhou" },
                                { label: "黄冈", value: "Huanggang" },
                                { label: "咸宁", value: "Xianning" },
                                { label: "随州", value: "Suizhou" },
                                { label: "恩施", value: "Enshi" },
                                { label: "仙桃", value: "Xiantao" },
                                { label: "潜江", value: "Qianjiang" },
                                { label: "天门", value: "Tianmen" },
                                { label: "神农架", value: "Shennongjia" }
                            ]
                    },
                    {
                        label: "湖南", value: "Hunan",
                        children:
                            [
                                { label: "长沙", value: "Changsha" },
                                { label: "株洲", value: "Zhuzhou" },
                                { label: "湘潭", value: "Xiangtan" },
                                { label: "衡阳", value: "Hengyang" },
                                { label: "邵阳", value: "Shaoyang" },
                                { label: "岳阳", value: "Yueyang" },
                                { label: "常德", value: "Changde" },
                                { label: "张家界", value: "Zhangjiajie" },
                                { label: "益阳", value: "Yiyang" },
                                { label: "郴州", value: "Chenzhou" },
                                { label: "永州", value: "Yongzhou" },
                                { label: "怀化", value: "Huaihua" },
                                { label: "娄底", value: "Loudi" },
                                { label: "湘西", value: "Xiangxi" }
                            ]
                    },
                    {
                        label: "广东", value: "Guangdong",
                        children:
                            [
                                { label: "广州", value: "Guangzhou" },
                                { label: "韶关", value: "Shaoguan" },
                                { label: "深圳", value: "Shenzhen" },
                                { label: "珠海", value: "Zhuhai" },
                                { label: "汕头", value: "Shantou" },
                                { label: "佛山", value: "Foshan" },
                                { label: "江门", value: "Jiangmen" },
                                { label: "湛江", value: "Zhanjiang" },
                                { label: "茂名", value: "Maoming" },
                                { label: "肇庆", value: "Zhaoqing" },
                                { label: "惠州", value: "Huizhou" },
                                { label: "梅州", value: "Meizhou" },
                                { label: "汕尾", value: "Shanwei" },
                                { label: "河源", value: "Heyuan" },
                                { label: "阳江", value: "Yangjiang" },
                                { label: "清远", value: "Qingyuan" },
                                { label: "东莞", value: "Dongguan" },
                                { label: "中山", value: "Zhongshan" },
                                { label: "潮州", value: "Chaozhou" },
                                { label: "揭阳", value: "Jieyang" },
                                { label: "云浮", value: "Yunfu" }
                            ]
                    },
                    {
                        label: "广西", value: "Guangxi",
                        children:
                            [
                                { label: "南宁", value: "Nanning" },
                                { label: "柳州", value: "Liuzhou" },
                                { label: "桂林", value: "Guilin" },
                                { label: "梧州", value: "Wuzhou" },
                                { label: "北海", value: "Beihai" },
                                { label: "防城港", value: "Fangchenggang" },
                                { label: "钦州", value: "Qinzhou" },
                                { label: "贵港", value: "Guigang" },
                                { label: "玉林", value: "Yulin" },
                                { label: "百色", value: "Baise" },
                                { label: "贺州", value: "Hezhou" },
                                { label: "河池", value: "Hechi" },
                                { label: "来宾", value: "Laibin" },
                                { label: "崇左", value: "Chongzuo" }
                            ]
                    },
                    {
                        label: "海南", value: "Hainan",
                        children:
                            [
                                { label: "海口", value: "Haikou" },
                                { label: "三亚", value: "Sanya" },
                                { label: "三沙", value: "Sansha" },
                                { label: "儋州", value: "Danzhou" },
                                { label: "五指山", value: "Wuzhishan" },
                                { label: "琼海", value: "Qionghai" },
                                { label: "文昌", value: "Wenchang" },
                                { label: "万宁", value: "Wanning" },
                                { label: "东方", value: "Dongfang" },
                                { label: "定安", value: "Ding'an" },
                                { label: "屯昌", value: "Tunchang" },
                                { label: "澄迈", value: "Chengmai" },
                                { label: "临高", value: "Lingao" },
                                { label: "白沙", value: "Baisha" },
                                { label: "昌江", value: "Changjiang" },
                                { label: "乐东", value: "Ledong" },
                                { label: "陵水", value: "Lingshui" },
                                { label: "保亭", value: "Baoting" },
                                { label: "琼中", value: "Qiongzhong" }
                            ]
                    },
                    {
                        label: "重庆", value: "Chongqing",
                        children:
                            [
                                { label: "重庆", value: "Chongqing" }
                            ]
                    },
                    {
                        label: "四川", value: "Sichuan",
                        children:
                            [
                                { label: "成都", value: "Chengdu" },
                                { label: "自贡", value: "Zigong" },
                                { label: "攀枝花", value: "Panzhihua" },
                                { label: "泸州", value: "Luzhou" },
                                { label: "德阳", value: "Deyang" },
                                { label: "遂宁", value: "Suining" },
                                { label: "内江", value: "Neijiang" },
                                { label: "乐山", value: "Leshan" },
                                { label: "南充", value: "Nanchong" },
                                { label: "眉山", value: "Meishan" },
                                { label: "宜宾", value: "Yibin" },
                                { label: "广安", value: "Guang'an" },
                                { label: "达州", value: "Dazhou" },
                                { label: "雅安", value: "Ya'an" },
                                { label: "巴中", value: "Bazhong" },
                                { label: "资阳", value: "Ziyang" },
                                { label: "阿坝", value: "Aba" },
                                { label: "甘孜", value: "Garze" },
                                { label: "凉山", value: "Liangshan" }
                            ]
                    },
                    {
                        label: "贵州", value: "Guizhou",
                        children:
                            [
                                { label: "贵阳", value: "Guiyang" },
                                { label: "六盘水", value: "Liupanshui" },
                                { label: "遵义", value: "Zunyi" },
                                { label: "安顺", value: "Anshun" },
                                { label: "毕节", value: "Bijie" },
                                { label: "铜仁", value: "Tongren" },
                                { label: "黔西南", value: "Qianxinan" },
                                { label: "黔东南", value: "Qiandongnan" },
                                { label: "黔南", value: "Qiannan" }
                            ]
                    },
                    {
                        label: "云南", value: "Yunnan",
                        children:
                            [
                                { label: "昆明", value: "Kunming" },
                                { label: "曲靖", value: "Qujing" },
                                { label: "玉溪", value: "Yuxi" },
                                { label: "保山", value: "Baoshan" },
                                { label: "昭通", value: "Zhaotong" },
                                { label: "丽江", value: "Lijiang" },
                                { label: "普洱", value: "Pu'er" },
                                { label: "临沧", value: "Lincang" },
                                { label: "楚雄", value: "Chuxiong" },
                                { label: "红河", value: "Honghe" },
                                { label: "文山", value: "Wenshan" },
                                { label: "西双版纳", value: "Xishuangbanna" },
                                { label: "大理", value: "Dali" },
                                { label: "德宏", value: "Dehong" },
                                { label: "怒江", value: "Nujiang" },
                                { label: "迪庆", value: "Deqen" }
                            ]
                    },
                    {
                        label: "西藏", value: "Tibet",
                        children:
                            [
                                { label: "拉萨", value: "Lhasa" },
                                { label: "日喀则", value: "Shigatse" },
                                { label: "昌都", value: "Qamdo" },
                                { label: "林芝", value: "Nyingchi" },
                                { label: "山南", value: "Shannan" },
                                { label: "那曲", value: "Nagqu" },
                                { label: "阿里", value: "Ngari" }
                            ]
                    },
                    {
                        label: "陕西", value: "Shaanxi",
                        children:
                            [
                                { label: "西安", value: "Xi'an" },
                                { label: "铜川", value: "Tongchuan" },
                                { label: "宝鸡", value: "Baoji" },
                                { label: "咸阳", value: "Xianyang" },
                                { label: "渭南", value: "Weinan" },
                                { label: "延安", value: "Yan'an" },
                                { label: "汉中", value: "Hanzhong" },
                                { label: "榆林", value: "Yulin" },
                                { label: "安康", value: "Ankang" },
                                { label: "商洛", value: "Shangluo" },
                                { label: "杨凌", value: "Yangling" }
                            ]
                    },
                    {
                        label: "甘肃", value: "Gansu",
                        children:
                            [
                                { label: "兰州", value: "Lanzhou" },
                                { label: "嘉峪关", value: "Jiayuguan" },
                                { label: "金昌", value: "Jinchang" },
                                { label: "白银", value: "Baiyin" },
                                { label: "天水", value: "Tianshui" },
                                { label: "武威", value: "Wuwei" },
                                { label: "张掖", value: "Zhangye" },
                                { label: "平凉", value: "Pingliang" },
                                { label: "酒泉", value: "Jiuquan" },
                                { label: "庆阳", value: "Qingyang" },
                                { label: "定西", value: "Dingxi" },
                                { label: "陇南", value: "Longnan" },
                                { label: "临夏", value: "Linxia" },
                                { label: "甘南", value: "Gannan" }
                            ]
                    },
                    {
                        label: "青海", value: "Qinghai",
                        children:
                            [
                                { label: "西宁", value: "Xining" },
                                { label: "海东", value: "Haidong" },
                                { label: "海北", value: "Haibei" },
                                { label: "黄南", value: "Huangnan" },
                                { label: "海南", value: "Hainan" },
                                { label: "果洛", value: "Golog" },
                                { label: "玉树", value: "Yushu" },
                                { label: "海西", value: "Haixi" }
                            ]
                    },
                    {
                        label: "宁夏", value: "Ningxia",
                        children:
                            [
                                { label: "银川", value: "Yinchuan" },
                                { label: "石嘴山", value: "Shizuishan" },
                                { label: "吴忠", value: "Wuzhong" },
                                { label: "固原", value: "Guyuan" },
                                { label: "中卫", value: "Zhongwei" },
                            ]
                    },
                    {
                        label: "新疆", value: "Xinjiang",
                        children:
                            [
                                { label: "乌鲁木齐", value: "Urumqi" },
                                { label: "克拉玛依", value: "Karamay" },
                                { label: "吐鲁番", value: "Turpan" },
                                { label: "哈密", value: "Hami" },
                                { label: "昌吉", value: "Changji" },
                                { label: "博尔塔拉", value: "Bortala" },
                                { label: "巴音郭楞", value: "Bayingolin" },
                                { label: "阿克苏", value: "Aksu" },
                                { label: "克孜勒苏", value: "Kizilsu" },
                                { label: "喀什", value: "Kashgar" },
                                { label: "和田", value: "Hotan" },
                                { label: "伊犁", value: "Ili" },
                                { label: "塔城", value: "Tacheng" },
                                { label: "阿勒泰", value: "Altay" },
                                { label: "石河子", value: "Shihezi" },
                                { label: "阿拉尔", value: "Aral" },
                                { label: "图木舒克", value: "Tumxuk" },
                                { label: "五家渠", value: "Wujiaqu" },
                                { label: "北屯", value: "Beitun" },
                                { label: "铁门关", value: "Tiemenguan" },
                                { label: "双河", value: "Shuanghe" },
                                { label: "可克达拉", value: "Kokdala" },
                                { label: "昆玉", value: "Kunyu" }
                            ]
                    },
                    {
                        label: "台湾", value: "Taiwan",
                        children:
                            [
                                { label: "台北", value: "Taipei"},
                                { label: "高雄", value: "Kaohsiung"},
                                { label: "台中", value: "Taichung"},
                                { label: "台南", value: "Tainan"},
                                { label: "新竹", value: "Hsinchu"},
                                { label: "嘉义", value: "Chiayi"},
                                { label: "宜兰", value: "Yilan"},
                                { label: "桃园", value: "Taoyuan"},
                                { label: "苗栗", value: "Miaoli"},
                                { label: "彰化", value: "Changhua"},
                                { label: "南投", value: "Nantou"},
                                { label: "云林", value: "Yunlin"},
                                { label: "屏东", value: "Pingtung"},
                                { label: "台东", value: "Taitung"},
                                { label: "花莲", value: "Hualien"},
                                { label: "澎湖", value: "Penghu"}
                            ]
                    },
                    {
                        label: "香港", value: "Hong Kong",
                        children:
                            [
                                { label: "香港", value: "Hong Kong"}
                            ]
                    },
                    {
                        label: "澳门", value: "Macau",
                        children:
                            [
                                { label: "澳门", value: "Macau"}
                            ]
                    }
                ],
                educationStage: [
                    {
                        label: "小学", value: "primary",
                        children: [
                            { label: "一年级", value: 1 },
                            { label: "二年级", value: 2 },
                            { label: "三年级", value: 3 },
                            { label: "四年级", value: 4 },
                            { label: "五年级", value: 5 },
                            { label: "六年级", value: 6 }
                        ]

                    },
                    {
                        label: "初中", value: "junior",
                        children: [
                            { label: "一年级", value: 7 }
                            , { label: "二年级", value: 8 }
                            , { label: "三年级", value: 9 }
                        ]
                    },
                    {
                        label: "高中", value: "senior",
                        children: [
                            { label: "一年级", value: 10 }
                            , { label: "二年级", value: 11 }
                            , { label: "三年级", value: 12 }
                        ]
                    },
                    {
                        label: "大学", value: "university",
                        children: [
                            { label: "大一", value: 13 }
                            , { label: "大二", value: 14 }
                            , { label: "大三", value: 15 }
                            , { label: "大四", value: 16 }
                            , { label: "研一", value: 17 }
                            , { label: "研二", value: 18 }
                            , { label: "研三", value: 19 }
                        ]
                    }
                ],
                educationStage_label: [
                    "如果你看到这句话，请联系管理员，谢谢！",
                    "小学一年级",
                    "小学二年级",
                    "小学三年级",
                    "小学四年级",
                    "小学五年级",
                    "小学六年级",
                    "初中一年级",
                    "初中二年级",
                    "初中三年级",
                    "高中一年级",
                    "高中二年级",
                    "高中三年级",
                    "大学大一",
                    "大学大二",
                    "大学大三",
                    "大学大四",
                    "研究生研一",
                    "研究生研二",
                    "研究生研三"
                ],
                identity: [
                    { label: "学生", value: 0 },
                    { label: "家教", value: 1 }
                ]
            };


export class identity {
    // Private Fields
    static #_teacher = 0;
    static #_student = 1;

    // Accessors for "get" functions only (no "set" functions
    static get teacher() { return this.#_teacher; }
    static get student() { return this.#_student; }
}
