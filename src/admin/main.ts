import { createApp } from 'vue'
import './style.css'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import 'element-plus/theme-chalk/dark/css-vars.css'
import { createMemoryHistory, createRouter } from 'vue-router'
import App from './App.vue'
import Users from './Users.vue'
import VueCookies from 'vue-cookies'


const routes = [
  { path: '/users', component: Users },
  { path: '', component: Users },
]

const router = createRouter({
  history: createMemoryHistory(),
  routes,
})

const app = createApp(App)
app.config.globalProperties.$cookies = VueCookies
app.use(VueCookies)
app.use(ElementPlus)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
app.use(router)
app.mount('#app')
