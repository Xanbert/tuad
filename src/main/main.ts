import { createApp } from 'vue'
import './style.css'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import 'element-plus/theme-chalk/dark/css-vars.css'
import { createMemoryHistory, createRouter } from 'vue-router'
import VueGravatar from "vue3-gravatar";
import App from './App.vue'
import Index from './Index.vue'
import Teachers from './Teachers.vue'
import TearcherInfo from './TeacherInfo.vue'
import recommend from './Recommend.vue'
import TeacherPublish from './TeacherPublish.vue'
import StudentPublish from './StudentPublish.vue'
import VueCookies from 'vue3-cookies'


const routes = [
  { path: '/', component: Index },
  { path: '/teachers', component: Teachers },
  { path: '/teacher-publish', component: TeacherPublish },
  { path: '/student-publish', component: StudentPublish },
  { path: '/teacher/:id', component: TearcherInfo },
  { path: '/recommend', component: recommend }
]

const router = createRouter({
  history: createMemoryHistory(),
  routes,
})

const app = createApp(App)
app.config.globalProperties.$cookies = VueCookies
app.use(VueCookies)
app.use(ElementPlus)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
app.use(router)
app.use(VueGravatar);
app.mount('#app')
