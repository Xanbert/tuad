import axios from 'axios';

// 注册
export const register = async (username, _class, sex, age, region, password, email) => {
  if (username == '' || _class === '' || sex === '' || age == '' || region == '' || password == '') {
    return null;
  }
  try {
    const params = new URLSearchParams();
    params.append('username', username);
    params.append('class', _class);
    params.append('sex', sex);
    params.append('age', age);
    params.append('region', region);
    params.append('password', password );
    params.append('email', email);
    const response = await axios.post('/api/register', params);
    if (response.status === 200) {
      return response.data;
      }
      else {
        return null;
      }
  } catch (error) {
    console.error(error);
    return null;
  }
};

// 登录
export const login = async (username, password) => {
  if (username == '' || password == '') {
    return null;
  }
  try {
    const params = new URLSearchParams();
    params.append('username', username);
    params.append('password', password);
    const response = await axios.post('/api/login', params);
    if (response.status === 200) {
      return response.data;
      }
      else {
        return null;
      }
  } catch (error) {
    console.error(error);
    return null;
  }
};

// 更新用户信息
export const userUpdate = async (userid, username, _class, sex, age, region, password) => {
  try {
    const params = new URLSearchParams();
    params.append('userid', userid);
    params.append('username', username);
    params.append('class', _class);
    params.append('sex', sex);
    params.append('age', age);
    params.append('region', region);
    params.append('password', password);
    const response = await axios.post('/api/userupdate', params);
    if (response.status === 200) {
      return response.data;
      }
      else {
        return null;
      }
  } catch (error) {
    console.error(error);
  }
};

// 删除用户
export const userDel = async (userid) => {
  try {
    const params = new URLSearchParams();
    params.append('userid', userid);
    const response = await axios.post('/api/userdel', params);
    if (response.status === 200) {
    return response.data;
    }
    else {
      return null;
    }
  } catch (error) {
    console.error(error);
  }
};

// 获取用户列表
export const userList = async () => {
  try {
    const response = await axios.post('/api/userlist');
    if (response.status === 200) {
      return response.data;
      }
      else {
        return null;
      }
  } catch (error) {
    console.error(error);
  }
};

// 添加家教信息
export const tutorAdd = async (userid, subject, educationstage, expprice, info) => {
  try {
    const params = new URLSearchParams();
    params.append('userid', userid);
    params.append('subject', subject);
    params.append('educationstage', educationstage);
    params.append('expprice', expprice);
    params.append('info', info);
    const response = await axios.post('/api/tutoradd', params);
    if (response.status === 200) {
      return response.data;
      }
      else {
        return null;
      }
  } catch (error) {
    console.error(error);
  }
};

// 获取家教信息列表
export const tutorList = async (subject, sex, region, educationstage, lowrate, lowprice, highprice) => {
  try {
    const params = new URLSearchParams();
    params.append('subject', subject);
    params.append('sex', sex);
    params.append('region', region);
    params.append('educationstage', educationstage);
    params.append('lowrate', lowrate);
    params.append('lowprice', lowprice);
    params.append('highprice', highprice);
    const response = await axios.post('/api/tutorlist', params);
    if (response.status === 200) {
      return response.data;
      }
      else {
        return null;
      }
  } catch (error) {
    console.error(error);
  }
};

// 获取家教信息
export const userInfo = async (userid) => {
  try {
    const params = new URLSearchParams();
    params.append('userid', userid);
    const response = await axios.post('/api/userinfo', params);
    if (response.status === 200) {
      return response.data;
      }
      else {
        return null;
      }
  } catch (error) {
    console.error(error);
  }
}

// 添加需求
export const requirementAdd = async (userid, subject, educationstage, time, info) => {
  try {
    const params = new URLSearchParams();
    params.append('userid', userid);
    params.append('subject', subject);
    params.append('educationstage', educationstage);
    params.append('time', time);
    params.append('info', info);
    const response = await axios.post('/api/requirementadd', params );
    if (response.status === 200) {
      return response.data;
      }
      else {
        return null;
      }
  } catch (error) {
    console.error(error);
  }
};

// 获取评分列表

export const tutorRate = async (userid) => {
  try {
    const params = new URLSearchParams();
    params.append('userid', userid);
    const response = await axios.post('/api/tutorrate', params);
    if (response.status === 200) {
      return response.data;
      }
      else {
        return null;
      }
  } catch (error) {
    console.error(error);
  }
};
// 添加评分

export const rateAdd = async (rateeid, raterid, rate, info) => {
  try {
    const params = new URLSearchParams();
    params.append('rateeid', rateeid);
    params.append('raterid', raterid);
    params.append('rate', rate);
    params.append('info', info);
    const response = await axios.post('/api/rateadd', params);
    if (response.status === 200) {
      return response.data;
      }
      else {
        return null;
      }
  } catch (error) {
    console.error(error);
  }
};

//删除评分
export const rateDel = async (raterid, rateeid) => {
  try {
    const params = new URLSearchParams();
    params.append('raterid', raterid);
    params.append('rateeid', rateeid);
    const response = await axios.post('/api/ratedel', params);
    if (response.status === 200) {
      return response.data;
      }
      else {
        return null;
      }
  } catch (error) {
    console.error(error);
  }
}

// 推荐
export const recommend = async (userid) => {
  try {
    const params = new URLSearchParams();
    params.append('userid', userid);
    const response = await axios.post('/api/recommend', params);
    if (response.status === 200) {
      return response.data;
      }
      else {
        return null;
      }
  } catch (error) {
    console.error(error);
  }
}
